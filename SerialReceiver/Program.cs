﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO.Ports;
using WindowsInput;
using WindowsInput.Native;

namespace SerialReceiver
{
    class Program
    {
        static SerialPort serial;
        static StringComparer stringComparer = StringComparer.OrdinalIgnoreCase;
        static bool running;

        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: SerialReceiver [port] [baudrate]");
                Environment.Exit(0);
            }

            // Initialization
            serial = new SerialPort(args[0], Convert.ToInt32(args[1]));
            Thread readThread = new Thread(Read);

            serial.ReadTimeout = 500;
            serial.WriteTimeout = 500;

            serial.Open();
            running = true;
            readThread.Start();

            Console.WriteLine("Type QUIT to exit");

            // Allow users to quit the application nicely
            while (running) {
                String message = Console.ReadLine();
                if (stringComparer.Equals("quit", message))
                    running = false;
            }

            // Clean up
            readThread.Join();
            serial.Close();

        }

        public static void Read()
        {
            InputSimulator input = new InputSimulator();
            while (running)
            {
                try
                {
                    string message = serial.ReadLine();

                    foreach(char char in message){
                        switch(char){
                            case '0' : input.Keyboard.KeyPress(VirtualKeyCode.VK_0); break;
                            case '1' : input.Keyboard.KeyPress(VirtualKeyCode.VK_1); break;
                            case '2' : input.Keyboard.KeyPress(VirtualKeyCode.VK_2); break;
                            case '3' : input.Keyboard.KeyPress(VirtualKeyCode.VK_3); break;
                            case '4' : input.Keyboard.KeyPress(VirtualKeyCode.VK_4); break;
                            case '5' : input.Keyboard.KeyPress(VirtualKeyCode.VK_5); break;
                            case '6' : input.Keyboard.KeyPress(VirtualKeyCode.VK_6); break;
                            case '7' : input.Keyboard.KeyPress(VirtualKeyCode.VK_7); break;
                            case '8' : input.Keyboard.KeyPress(VirtualKeyCode.VK_8); break;
                            case '9' : input.Keyboard.KeyPress(VirtualKeyCode.VK_9); break;
                            case 'A' : input.Keyboard.KeyPress(VirtualKeyCode.VK_A); break;
                            case 'B' : input.Keyboard.KeyPress(VirtualKeyCode.VK_B); break;
                            case 'C' : input.Keyboard.KeyPress(VirtualKeyCode.VK_C); break;
                            case 'D' : input.Keyboard.KeyPress(VirtualKeyCode.VK_D); break;
                            case 'E' : input.Keyboard.KeyPress(VirtualKeyCode.VK_E); break;
                            case 'F' : input.Keyboard.KeyPress(VirtualKeyCode.VK_F); break;
                            default : break;
                        }
                    }
                }

                catch(TimeoutException) { }
            }
        }
    }
}
